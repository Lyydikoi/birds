package com.example.birds.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.birds.data.entities.Bird
import com.example.birds.db.daos.BirdDao
import com.example.birds.utils.DATA_BASE_VERSION

@Database(entities = [Bird::class], version = DATA_BASE_VERSION, exportSchema = false)
abstract class LintuineDb : RoomDatabase() {

    abstract fun birdDao(): BirdDao

}