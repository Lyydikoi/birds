package com.example.birds.db.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.birds.data.entities.Bird

@Dao
interface BirdDao {
    @Query("SELECT * from bird_table ORDER BY created_at DESC")
    fun getAllBirds(): LiveData<List<Bird>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(bird: Bird)

    @Update
    suspend fun update(bird: Bird)

    @Delete
    suspend fun delete(bird: Bird)
}