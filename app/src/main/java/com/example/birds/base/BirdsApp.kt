package com.example.birds.base

import android.app.Application
import com.example.birds.di.appModule
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BirdsApp : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        // Start Koin
        startKoin {
            androidLogger()
            androidContext(this@BirdsApp)
            modules(appModule)
        }
    }

}