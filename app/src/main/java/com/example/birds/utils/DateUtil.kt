package com.example.birds.utils

import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.chrono.IsoChronology
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeFormatterBuilder
import org.threeten.bp.format.FormatStyle
import java.util.*

class DateUtil {
    companion object {
        fun getShortMonthYearAndDayFromFullPattern(date: String): String {
            return try {
                val billDate =
                    LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                val formatterPattern =
                    DateTimeFormatterBuilder.getLocalizedDateTimePattern(
                        FormatStyle.MEDIUM,
                        null,
                        IsoChronology.INSTANCE,
                        Locale.getDefault()
                    )

                DateTimeFormatter.ofPattern(formatterPattern).format(billDate)
            } catch (e: Exception) {
                e.printStackTrace()
                ""
            }
        }

        fun millisecToReadableHoursMinutes(millisec: Long): String {
            return try {
                val instant = Instant.ofEpochMilli(millisec)
                val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                    .withLocale(Locale.getDefault())
                    .withZone(ZoneId.systemDefault())
                formatter.format(instant)
            } catch (e: Exception) {
                e.printStackTrace()
                ""
            }
        }
    }
}