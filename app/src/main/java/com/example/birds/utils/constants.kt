package com.example.birds.utils

// Database
const val DATA_BASE_VERSION = 1
const val DATA_BASE_NAME = "lintuine.db"
const val BIRD_BUNDLED = "bird_bundled"

// Keys
const val RARE = "rare"
const val COMMON = "common"
const val UNIQUE = "unique"

// Request Codes
const val GALLERY_REQUEST_CODE = 99

// Location
const val REQUEST_CHECK_SETTINGS = 11111
const val LOCATION_REQUEST_INTERVAL: Long = 1000