package com.example.birds.utils

import java.lang.Exception
import java.lang.StringBuilder

class StringUtil {
    companion object {
        fun getCoordString(lat: Double?, lon: Double?): String {
            return try {
                val stringBuilder = StringBuilder()
                stringBuilder.append(lat?.let { it.toString() } ?: "").append(" ").append(lon?.let { it.toString() } ?: "")
                stringBuilder.toString()
            } catch (e: Exception) {
                e.printStackTrace()
                ""
            }
        }
    }
}