package com.example.birds.data.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "bird_table")
data class Bird(
    val species: String,
    val rarity: String,
    val notes: String,
    val lat: Double?,
    val lon: Double?,
    var imgUriString: String?,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "created_at")
    var createdAt: Long,
    @ColumnInfo(name = "updated_at")
    val updatedAt: Long
) : Parcelable
