package com.example.birds.data.repositories

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.birds.data.entities.Bird

interface BirdRepository {

    val allBirds: LiveData<List<Bird>>

    val selectedImagePath: MutableLiveData<String>

    val userLocation: MutableLiveData<Location>

    suspend fun insert(bird: Bird)

    suspend fun update(bird: Bird)

    suspend fun delete(bird: Bird)

}