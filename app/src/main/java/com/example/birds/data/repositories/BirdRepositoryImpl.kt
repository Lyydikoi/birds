package com.example.birds.data.repositories

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.birds.data.entities.Bird
import com.example.birds.db.daos.BirdDao


class BirdRepositoryImpl(private val birdDao: BirdDao) : BirdRepository {

    override val allBirds: LiveData<List<Bird>> = birdDao.getAllBirds()

    override val selectedImagePath by lazy {
        MutableLiveData<String>()
    }

    override val userLocation by lazy {
        MutableLiveData<Location>()
    }

    override suspend fun insert(bird: Bird) {
        birdDao.insert(bird)
    }

    override suspend fun update(bird: Bird) {
        birdDao.update(bird)
    }

    override suspend fun delete(bird: Bird) {
        birdDao.delete(bird)
    }

}