package com.example.birds.di

import androidx.room.Room
import com.example.birds.data.repositories.BirdRepository
import com.example.birds.data.repositories.BirdRepositoryImpl
import com.example.birds.db.LintuineDb
import com.example.birds.ui.MainActivityViewModel
import com.example.birds.ui.birddetail.BirdDetailsViewModel
import com.example.birds.ui.birdslist.BirdsViewModel
import com.example.birds.utils.DATA_BASE_NAME
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {
    single {
        Room
            .databaseBuilder(androidContext(), LintuineDb::class.java, DATA_BASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    single {
        get<LintuineDb>().birdDao()
    }

    single<BirdRepository> {
        BirdRepositoryImpl(get())
    }

    viewModel {
        BirdsViewModel(get())
    }

    viewModel {
        BirdDetailsViewModel(get())
    }

    viewModel {
        MainActivityViewModel(get())
    }

}