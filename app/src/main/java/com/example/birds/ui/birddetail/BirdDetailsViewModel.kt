package com.example.birds.ui.birddetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.birds.data.entities.Bird
import com.example.birds.data.repositories.BirdRepository
import com.example.birds.utils.Event
import kotlinx.coroutines.launch

class BirdDetailsViewModel(
    private val birdRepository: BirdRepository
) : ViewModel() {

    private val mldLoading by lazy { MutableLiveData<Boolean>() }
    var ldLoading = mldLoading

    private val mldSelectedImagePath = birdRepository.selectedImagePath
    val ldSelectedImagePath = Transformations.map(mldSelectedImagePath) {
        Event(it)
    }

    private val mldUserLocation = birdRepository.userLocation
    val ldUserLocation = mldUserLocation

    fun update(bird: Bird) {
        viewModelScope.launch {
            mldLoading.value = true
            birdRepository.update(bird)
            mldLoading.value = false
        }
    }

    fun create(bird: Bird) {
        viewModelScope.launch {
            mldLoading.value = true
            birdRepository.insert(bird)
            mldLoading.value = false
        }
    }

    fun resetSelectedImagePath() {
        birdRepository.selectedImagePath.value = null
    }
}