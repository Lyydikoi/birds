package com.example.birds.ui.birdslist

import androidx.lifecycle.*
import com.example.birds.data.entities.Bird
import com.example.birds.data.repositories.BirdRepository

class BirdsViewModel(
    private val birdsRepository: BirdRepository
) : ViewModel() {
    private val ldIsReversedSorting by lazy { MutableLiveData<Boolean>(false) }
    private val ldBirdsList = birdsRepository.allBirds

    private val medSortedBirds: MediatorLiveData<MutableList<Bird>> = prepareSortedMediator()
    var ldSortedBirdsList: LiveData<MutableList<Bird>> = medSortedBirds

    fun getSelectedBird(position: Int): Bird? {
        ldSortedBirdsList.value?.let {
          return it[position]
        } ?: return null
    }

    fun setIsReversedSorting(isReversed: Boolean) {
        ldIsReversedSorting.value = isReversed
    }

    // This makes observing of both sources: sorting type and birds list. If any changed, list is sorted.
    private fun prepareSortedMediator(): MediatorLiveData<MutableList<Bird>> {
        val result = MediatorLiveData<MutableList<Bird>>()

        result.addSource(ldBirdsList) {
            result.value = sort(ldIsReversedSorting, ldBirdsList)
        }
        result.addSource(ldIsReversedSorting) {
            result.value = sort(ldIsReversedSorting, ldBirdsList)
        }
        return result
    }

    private fun sort(
        ldIsReversedSorting: MutableLiveData<Boolean>,
        ldBirdsList: LiveData<List<Bird>>
    ): MutableList<Bird>? {
        ldIsReversedSorting.value?.let {
            return if (it) {
                ldBirdsList.value?.asReversed()?.toMutableList()
            } else {
                ldBirdsList.value?.toMutableList()
            }
        } ?: return null
    }

}