package com.example.birds.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.birds.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.net.Uri
import android.os.Looper
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.birds.utils.LOCATION_REQUEST_INTERVAL
import com.example.birds.utils.REQUEST_CHECK_SETTINGS
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import lv.chi.photopicker.PhotoPickerFragment

class MainActivity : AppCompatActivity(), PhotoPickerFragment.Callback {

    private val locationRequest by lazy { LocationRequest() }
    private val fusedLocationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }

    private val viewModel by viewModel<MainActivityViewModel>()
    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView(this, com.example.birds.R.layout.activity_main) as ActivityMainBinding
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            locationResult?.let {
                it.locations[0]?.let { location ->
                    viewModel.setUserLocation(location)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        requestLocation()
    }

    override fun onImagesPicked(photos: ArrayList<Uri>) {
        if (photos.isNotEmpty()) {
            viewModel.saveSelectedImagePath(photos.first().toString())
        }
    }

    // All location related things go below.
    @SuppressLint("MissingPermission")
    private fun initLocationUpdates() {
        fusedLocationClient.lastLocation.addOnSuccessListener {
            it?.let { location ->
                viewModel.setUserLocation(location)
            }

            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
        }
    }

    private fun requestLocation() {
        if (locationPermissionGratned()) {
            setUpLocationRequest()
            getLocationSettings()?.let {
                it.addOnSuccessListener {
                    // All location settings are satisfied.
                    initLocationUpdates()
                }
                it.addOnFailureListener { exception ->
                    if (exception is ResolvableApiException){
                        // Location settings are not satisfied, but this can be fixed by showing the user a dialog.
                        try {
                            exception.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                        } catch (sendEx: IntentSender.SendIntentException) {
                            sendEx.printStackTrace()
                        }
                    }
                }
            }
        }
    }

    private fun locationPermissionGratned(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 9)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 9) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocation()
            }
        }
    }

    private fun setUpLocationRequest() {
        locationRequest.interval = LOCATION_REQUEST_INTERVAL
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun getLocationSettings(): Task<LocationSettingsResponse>? {
        val settingsBuilder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val settingsClient: SettingsClient = LocationServices.getSettingsClient(this)
        return  settingsClient.checkLocationSettings(settingsBuilder.build())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_OK) {
            requestLocation()
        }
    }

}
