package com.example.birds.ui.birddetail

import android.app.AlertDialog
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.birds.R
import com.example.birds.data.entities.Bird
import com.example.birds.databinding.FragmenBirdDetailstBinding
import com.example.birds.ui.MainActivity
import com.example.birds.utils.*
import com.example.birds.utils.loaders.GlideImageLoader
import lv.chi.photopicker.PhotoPickerFragment
import org.koin.androidx.viewmodel.ext.android.viewModel


class BirdDetailsFragment : Fragment() {
    private lateinit var binding: FragmenBirdDetailstBinding
    private val viewModel by viewModel<BirdDetailsViewModel>()
    private var selectedBird: Bird? = null
    private var selectedImageUriString: String? = null
    private var selectedRarity: String = COMMON
    private var userLocation: Location? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmenBirdDetailstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.ldLoading.observe(viewLifecycleOwner, Observer {
            binding.pbLoading.visibility = if (it) VISIBLE else GONE
        })

        viewModel.ldSelectedImagePath.observe(viewLifecycleOwner, Observer { event ->
            // Observed only once as single event (each time, when new value assigned)
            val data = event.getContentIfNotHandled()
            data?.let { uriString ->
                selectedImageUriString = uriString
                context?.let{c ->
                    GlideImageLoader().loadImage(c, binding.ivAvatar, Uri.parse(uriString))
                }
            }
        })

        viewModel.ldUserLocation.observe(viewLifecycleOwner, Observer {
            userLocation = it
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            it.getParcelable<Bird>(BIRD_BUNDLED)?.let { bird ->
                selectedBird = bird
                showBirdInfo(bird)
            }
        }

        binding.btnCancel.setOnClickListener {
            activity?.onBackPressed()
        }

        binding.btnSaveDetails.setOnClickListener {
            if (!allFieldsValid()) {
                Toast.makeText(context, resources.getString(R.string.nothing_changed), Toast.LENGTH_SHORT).show()
            } else {
                AlertDialog.Builder(context, R.style.RoundDialogTheme)
                    .setMessage(resources.getString(R.string.should_save_changes))
                    .setNegativeButton(R.string.btn_cancel) { dialog, _ -> dialog.dismiss() }
                    .setPositiveButton(R.string.btn_yes) { _, _ -> saveDetails() }
                    .create().show()
            }
        }

        binding.sbRarity.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(p0: SeekBar?) { /* Ignored. */ }

            override fun onStopTrackingTouch(p0: SeekBar?) { /* Ignored. */ }

            override fun onProgressChanged(p0: SeekBar?, progress: Int, p2: Boolean) {
                handleRarityChanged(progress)
            }
        })

        binding.ivAvatar.setOnClickListener {
            openPicker()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.resetSelectedImagePath()
    }

    private fun openPicker() {
        activity?.let { a ->
            context?.let { c ->
                PhotoPickerFragment.newInstance(multiple = false, allowCamera = true)
                    .imageLoader(GlideImageLoader())
                    .setTheme(R.style.ChiliPhotoPicker_Dark)
                    .authority(c.packageName + ".fileprovider")
                    .show(a.supportFragmentManager, "picker") // callback will go to main activity
            }
        }
    }

    private fun showBirdInfo(bird: Bird) {
        binding.btnSaveDetails.text = resources.getString(R.string.save)
        if (bird.notes.isNotBlank()) binding.edtNotes.setText(bird.notes)
        if (bird.species.isNotBlank()) binding.edtSpecies.setText(bird.species)
        if (bird.rarity.isNotBlank()) {
            selectedRarity = bird.rarity
            setRarityText()
            setRarityProgress()
        }
        context?.let{c ->
            GlideImageLoader().loadImage(c, binding.ivAvatar, Uri.parse(bird.imgUriString?.let { it } ?: ""))
        }
    }

    private fun handleRarityChanged(progress: Int) {
        val value = when(progress) {
            in 0..29 -> COMMON
            in 30..59 -> RARE
            else -> UNIQUE
        }
        selectedRarity = value
        setRarityText()
    }

    private fun setRarityText() {
        when(selectedRarity) {
            UNIQUE -> binding.edtRarity.text = resources.getString(R.string.unique)
            RARE -> binding.edtRarity.text = resources.getString(R.string.rare)
            COMMON-> binding.edtRarity.text = resources.getString(R.string.common)
        }
    }

    private fun setRarityProgress() {
        when(selectedRarity) {
            COMMON -> binding.sbRarity.progress = 15
            RARE -> binding.sbRarity.progress = 45
            UNIQUE -> binding.sbRarity.progress = 75
        }
    }

    private fun saveDetails() {
        selectedBird?.let {
            viewModel.update(getBirdData()) // update existing
        } ?: viewModel.create(getBirdData()) // create new
        activity?.onBackPressed()
    }

    private fun allFieldsValid(): Boolean {
        selectedBird?.let { it ->
            return binding.edtSpecies.text.toString() != it.species ||
                    binding.edtNotes.text.toString() != it.notes ||
                    it.rarity != selectedRarity ||
                    selectedImageUriString != null
        } ?: return binding.edtSpecies.text.isNotBlank() ||
                binding.edtNotes.text.isNotBlank() ||
                selectedRarity != COMMON ||
                selectedImageUriString != null
    }

    private fun getBirdData(): Bird {
        return Bird(
            if (binding.edtSpecies.text.isNotBlank()) binding.edtSpecies.text.toString() else "",
            selectedRarity,
            if (binding.edtNotes.text.isNotBlank()) binding.edtNotes.text.toString() else "",
            userLocation?.let { it.latitude },
            userLocation?.let { it.longitude },
            selectedImageUriString?.let { selectedImageUriString } ?: selectedBird?.imgUriString,
            selectedBird?.let { it.createdAt } ?: System.currentTimeMillis(),
            System.currentTimeMillis()
        )
    }

}
