package com.example.birds.ui

import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.birds.data.entities.Bird
import com.example.birds.data.repositories.BirdRepository
import kotlinx.coroutines.launch

class MainActivityViewModel(
    private val birdRepository: BirdRepository
) : ViewModel() {

    fun saveSelectedImagePath(imgDecodableString: String){
        birdRepository.selectedImagePath.value = imgDecodableString
    }

    fun setUserLocation(location: Location) {
        birdRepository.userLocation.value = location
    }

}