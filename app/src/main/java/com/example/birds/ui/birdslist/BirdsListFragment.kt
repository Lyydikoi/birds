package com.example.birds.ui.birdslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.birds.R
import com.example.birds.data.entities.Bird
import com.example.birds.databinding.FragmentBirdsListBinding
import com.example.birds.ui.birdslist.adapter.BirdsListAdapter
import com.example.birds.utils.BIRD_BUNDLED
import org.koin.android.ext.android.bind
import org.koin.androidx.viewmodel.ext.android.viewModel

class BirdsListFragment : Fragment() {

    private var isReversedSorting = false
    private lateinit var binding: FragmentBirdsListBinding
    private val viewModel by viewModel<BirdsViewModel>()
    private val adapter by lazy {
        BirdsListAdapter(object :
            BirdsListAdapter.Interaction {
            override fun itemClicked(position: Int) {
                val selectedBird = viewModel.getSelectedBird(position)
                selectedBird?.let {
                    findNavController().navigate(
                        R.id.action_birdsListFragment_to_birdDetailsFragment,
                        bundleOf(BIRD_BUNDLED to it))
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBirdsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.ldSortedBirdsList.observe(viewLifecycleOwner, Observer { birds ->
            birds?.let {
                adapter.submitList(birds)
                adapter.notifyItemChanged(birds.lastIndex)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvBirds.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.rvBirds.adapter = adapter
        binding.btnAddBird.setOnClickListener {
            findNavController().navigate(R.id.action_birdsListFragment_to_birdDetailsFragment)
        }
        binding.ivBirdsOrder.setOnClickListener {
            isReversedSorting = !isReversedSorting
            viewModel.setIsReversedSorting(isReversedSorting)
        }
    }

}
