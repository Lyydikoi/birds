package com.example.birds.ui.birdslist.adapter

import android.net.Uri
import androidx.recyclerview.widget.RecyclerView
import com.example.birds.data.entities.Bird
import com.example.birds.databinding.LayoutBirdViewHolderBinding
import com.example.birds.utils.DateUtil.Companion.millisecToReadableHoursMinutes
import com.example.birds.utils.StringUtil.Companion.getCoordString
import com.example.birds.utils.loaders.GlideImageLoader

class BirdViewHolder(
    private val binding: LayoutBirdViewHolderBinding,
    private val interaction: BirdsListAdapter.Interaction?
) : RecyclerView.ViewHolder(binding.root) {

    init {
        itemView.setOnClickListener{
            if (adapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
            interaction?.itemClicked(adapterPosition)
        }
    }

    fun bind(item: Bird) {
        binding.edtSpecies.text = item.species
        binding.edtRarity.text = item.rarity
        binding.tvLocation.text = getCoordString(item.lat, item.lon)
        item.createdAt?.let {
            binding.tvTime.text = millisecToReadableHoursMinutes(it)
        }
        binding.tvNotes.text = item.notes
        item.imgUriString?.let {
            GlideImageLoader().loadImage(binding.root.context, binding.ivPicture, Uri.parse(it))
        }

    }
}