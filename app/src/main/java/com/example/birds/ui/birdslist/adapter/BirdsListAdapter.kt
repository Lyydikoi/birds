package com.example.birds.ui.birdslist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.birds.data.entities.Bird
import com.example.birds.databinding.LayoutBirdViewHolderBinding

class BirdsListAdapter(private val interaction: Interaction? = null) :
    ListAdapter<Bird, BirdViewHolder>(BirdDU()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        BirdViewHolder(
            LayoutBirdViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            interaction
        )

    override fun onBindViewHolder(holder: BirdViewHolder, position: Int) =
        holder.bind(getItem(position))

    interface Interaction {
        fun itemClicked(position: Int)
    }

    private class BirdDU : DiffUtil.ItemCallback<Bird>() {
        override fun areItemsTheSame(oldItem: Bird, newItem: Bird): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Bird, newItem: Bird): Boolean {
            return oldItem.updatedAt == newItem.updatedAt
        }
    }
}