package data.source

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.birds.data.entities.Bird
import com.example.birds.data.repositories.BirdRepository
import data.TestDataFactory

class FakeBirdRepository : BirdRepository {
    private val birdServiceData by lazy { LinkedHashMap<Long, Bird>() }

    override val allBirds: LiveData<List<Bird>>
        get() = returnAllUsers()

    override val selectedImagePath by lazy { MutableLiveData<String>() }

    override val userLocation by lazy { MutableLiveData<Location>() }

    override suspend fun insert(bird: Bird) {
        birdServiceData[bird.createdAt] = bird
    }

    override suspend fun update(bird: Bird) {
        birdServiceData[bird.createdAt] = bird
    }

    override suspend fun delete(bird: Bird) {
        birdServiceData.remove(bird.createdAt)
    }

    private fun returnAllUsers() : LiveData<List<Bird>> {
        val resultList = mutableListOf<Bird>()
        birdServiceData.forEach { (index, value) -> resultList.add(value) }
        val result = MutableLiveData<List<Bird>>()
        result.value = resultList
        return result
    }

}