package data

import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.example.birds.data.entities.Bird

object TestDataFactory {

    fun getBird(name: String, orderNum: Int) : Bird {
        return Bird(
            "Species $name",
            "rare",
            "Notes $name",
            60.167213,
            24.930698,
            "uri",
            1574612549887 + orderNum,
            1574612549890 + orderNum
        )
    }

    fun getHelsinkiLocation(): Location {
        val location = Location("")
        location.latitude = 60.167213
        location.longitude = 24.930698
        return location
    }

}