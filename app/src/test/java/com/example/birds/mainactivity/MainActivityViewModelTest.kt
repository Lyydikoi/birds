package com.example.birds.mainactivity

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.birds.LiveDataTestUtil.getValue
import com.example.birds.ui.MainActivityViewModel
import com.google.common.truth.Truth
import data.TestDataFactory
import data.source.FakeBirdRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * These tests are run locally on development machine's JVM and do not require running on either
 * an emulator or physical device.
 * Unit tests are almost always local tests.
 * Integration tests can be run locally or as instrumented tests.
 *
 * Run instructions: src -> test -> ... -> right click on class MainActivityViewModelTest -> click "Run 'MainActivityViewModelTest'"
 */
class MainActivityViewModelTest {

    /**
     * Unit tests for [ui.MainActivityViewModel]
     */

    // Subject under test
    private lateinit var mainActivityViewModel: MainActivityViewModel

    // Depended On Components
    private lateinit var birdRepository: FakeBirdRepository

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUpViewModel() {
        birdRepository = FakeBirdRepository()
        mainActivityViewModel = MainActivityViewModel(birdRepository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun saveImagePathAndNotifyView() {
        val imagePath = "uri1"
        mainActivityViewModel.saveSelectedImagePath(imagePath)
        Truth.assertThat(getValue(birdRepository.selectedImagePath)).isEqualTo(imagePath)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun saveUserLocationNotifyView() {
        val userLocation = TestDataFactory.getHelsinkiLocation()
        mainActivityViewModel.setUserLocation(userLocation)
        Truth.assertThat(getValue(birdRepository.userLocation)).isEqualTo(userLocation)
    }

}