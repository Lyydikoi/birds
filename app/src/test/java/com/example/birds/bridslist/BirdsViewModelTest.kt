package com.example.birds.bridslist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.birds.LiveDataTestUtil.getValue
import com.example.birds.ui.birdslist.BirdsViewModel
import data.TestDataFactory
import data.source.FakeBirdRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import com.google.common.truth.Truth.assertThat
import data.MainCoroutineRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * These tests are run locally on development machine's JVM and do not require running on either
 * an emulator or physical device.
 * Unit tests are almost always local tests.
 * Integration tests can be run locally or as instrumented tests.
 *
 * Run instructions: src -> test -> ... -> right click on class BirdsViewModelTest -> click "Run 'BirdsViewModelTest'"
 */
class BirdsViewModelTest {

    /**
     * Unit tests for [ui.birdslist.BirdsViewModel]
     */

    // Subject under test
    private lateinit var birdsViewModel: BirdsViewModel

    // Depended On Components
    private lateinit var birdRepository: FakeBirdRepository

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUpViewModel() {
        birdRepository = FakeBirdRepository()
        birdsViewModel = BirdsViewModel(birdRepository)
    }


    @ExperimentalCoroutinesApi
    @Test
    fun getBirdsListFromRepoAndNotifyView() {
        assertThat(birdsViewModel.ldSortedBirdsList.value).isNull()

        mainCoroutineRule.launch(Dispatchers.IO) {
            birdRepository.insert(TestDataFactory.getBird("Bird one", 1))
            birdRepository.insert(TestDataFactory.getBird("Bird two", 2))
            birdRepository.insert(TestDataFactory.getBird("Bird three", 3))

            // Verify that the view has been notified and size of list is right.
            assertThat(getValue(birdsViewModel.ldSortedBirdsList)).isNotEmpty()
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).size).isEqualTo(3)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun birdsListIsSortedDescendByDefault() {
        mainCoroutineRule.launch(Dispatchers.IO) {
            birdRepository.insert(TestDataFactory.getBird("Bird one", 1))
            birdRepository.insert(TestDataFactory.getBird("Bird two", 2))
            birdRepository.insert(TestDataFactory.getBird("Bird three", 3))

            // Verify that the view has been notified and list is sorted as expected.
            assertThat(getValue(birdsViewModel.ldSortedBirdsList)).isNotEmpty()
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).first().species).isEqualTo("Bird one")
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).last().species).isEqualTo("Bird three")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun sortingWorksWhenIsReversedSortingChanged() {
        mainCoroutineRule.launch(Dispatchers.IO) {
            birdRepository.insert(TestDataFactory.getBird("Bird one", 1))
            birdRepository.insert(TestDataFactory.getBird("Bird two", 2))
            birdRepository.insert(TestDataFactory.getBird("Bird three", 3))

            // Result is sorted descent by default.
            assertThat(getValue(birdsViewModel.ldSortedBirdsList)).isNotEmpty()
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).first().species).isEqualTo("Bird one")
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).last().species).isEqualTo("Bird three")

            // Change sorting rule
            birdsViewModel.setIsReversedSorting(true)

            // Verify that the view has been notified and list is sorted as expected.
            assertThat(getValue(birdsViewModel.ldSortedBirdsList)).isNotEmpty()
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).first().species).isEqualTo("Bird three")
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).last().species).isEqualTo("Bird one")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun sortingWorksWhenBirdsListChangedFromRepo() {
        mainCoroutineRule.launch(Dispatchers.IO) {
            birdRepository.insert(TestDataFactory.getBird("Bird one", 1))
            birdRepository.insert(TestDataFactory.getBird("Bird two", 2))
            birdRepository.insert(TestDataFactory.getBird("Bird three", 3))

            // Result is sorted descent by default.
            assertThat(getValue(birdsViewModel.ldSortedBirdsList)).isNotEmpty()
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).first().species).isEqualTo("Bird one")
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).last().species).isEqualTo("Bird three")

            // Change sorting rule
            birdsViewModel.setIsReversedSorting(true)

            // Verify that the view has been notified and list is sorted as expected.
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).first().species).isEqualTo("Bird three")
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).last().species).isEqualTo("Bird one")

            birdRepository.insert(TestDataFactory.getBird("Bird four", 4))
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).first().species).isEqualTo("Bird four")
            assertThat(getValue(birdsViewModel.ldSortedBirdsList).last().species).isEqualTo("Bird one")
        }
    }

}