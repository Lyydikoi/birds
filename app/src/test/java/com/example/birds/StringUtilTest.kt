package com.example.birds

import com.example.birds.utils.StringUtil.Companion.getCoordString
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

/**
 * These tests are run locally on development machine's JVM and do not require running on either
 * an emulator or physical device.
 * Unit tests are almost always local tests.
 * Integration tests can be run locally or as instrumented tests.
 *
 * Run instructions: src -> test -> ... -> right click on class StringUtilTest -> click "Run 'StringUtilTest'"
 */
class StringUtilTest {

    /**
     * Unit tests for [StringUtil.getCoordString]
     */

    @Test
    fun getCoordString_blank() {
        val lat = null
        val lng = null
        val result = getCoordString(lat, lng)
        assertThat(result, `is` (" "))
    }

    @Test
    fun getCoordString_valid() {
        val lat = 60.167213
        val lng = 24.930698
        val result = getCoordString(lat, lng)
        assertThat(result, `is` ("60.167213 24.930698"))
    }


}