package com.example.birds

import com.example.birds.utils.DateUtil.Companion.millisecToReadableHoursMinutes
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

/**
 * These tests are run locally on development machine's JVM and do not require running on either
 * an emulator or physical device.
 * Unit tests are almost always local tests.
 * Integration tests can be run locally or as instrumented tests.
 *
 * Run instructions: src -> test -> ... -> right click on class DateUtilTest -> click "Run 'DateUtilTest'"
 */
class DateUtilTest {

    /**
     * Unit tests for [DateUtil.millisecToReadableHoursMinutes]
     */

    @Test
    fun millisecToReadableHoursMinutes_valid() {
        val milliseconds = 1574606044878
        val result = millisecToReadableHoursMinutes(milliseconds)
        assertThat(result, `is` ("11/24/19 4:34 PM"))
    }


}